# Al modificar el nombre del stack hay q hacer la imagen del data-service nuevamente modificando la linea 15 de database.go
echo "- Cleaning up API volume..."
docker volume rm sayri_data_service_alarms_api

echo
echo "- Deploying stack 'sayri_data_service_alarms'..."
docker stack deploy -c docker-compose.yml sayri_data_service_alarms

echo
sleep 5s
docker service ls

echo
echo "- Done!"
